FROM aquasec/trivy:0.24.2

COPY pipe /

RUN apk --no-cache add bash=5.1.16-r0 && chmod +x /pipe.sh

ENTRYPOINT ["/pipe.sh"] 
